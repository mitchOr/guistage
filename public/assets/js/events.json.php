{
	"success": 1,
	"result": [
		{
			"id": "293",
			"title": "This is warning class event with very long title to check how it fits to evet in day view",
			"url": "#",
			"class": "event-warning",
			"start": "1553151600000",
			"end":   "1553158800000"
		},
		{
			"id": "256",
			"title": "Event that ends on timeline",
			"url": "#",
			"class": "event-warning",
			"start": "1553162400000",
			"end":   "1553166000000"
		},
		{
			"id": "276",
			"title": "Short day event",
			"url": "#",
			"class": "event-success",
			"start": "1553245200000",
			"end":   "1553252400000"
		},
		{
			"id": "294",
			"title": "This is information class ",
			"url": "#",
			"class": "event-info",
			"start": "1553346000000",
			"end":   "1553353200000"
		},
		{
			"id": "297",
			"title": "This is success event",
			"url": "#",
			"class": "event-success",
			"start": "1363234500000",
			"end":   "1363284062400"
		},
		{
			"id": "54",
			"title": "This is simple event",
			"url": "#",
			"class": "",
			"start": "1553356800000",
			"end":   "1553360400000"
		},
		{
			"id": "532",
			"title": "This is inverse event",
			"url": "#",
			"class": "event-inverse",
			"start": "1553504400000",
			"end":   "1553515200000"
		},
		{
			"id": "548",
			"title": "This is special event",
			"url": "#",
			"class": "event-special",
			"start": "1553601600000",
			"end":   "1553608800000"
		},
		{
			"id": "295",
			"title": "Event 3",
			"url": "#",
			"class": "event-important",
			"start": "1553781600000",
			"end":   "1553788800000"
		}
	]
}

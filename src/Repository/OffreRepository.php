<?php

namespace App\Repository;

use App\Entity\Offre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Offre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Offre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Offre[]    findAll()
 * @method Offre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OffreRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Offre::class);
    }

/**
 * @return Offre[] Returns an array of Offre objects
 */ 
    public function findByField($ville, $titre)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.lieu like :val')
            ->setParameter('val', $ville)
            ->andWhere('o.titre like :val2')
            ->setParameter('val2', $titre)
            ->orderBy('o.date', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;

      
    }
    

    /**
    * @return Offre[] Returns an array of Offre objects
    */
    public function findAllByTitre($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.titre like :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return Offre[] Returns an array of Offre objects
    */
    public function findAllByLieu($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.lieu like :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
        ;
    }
    
}

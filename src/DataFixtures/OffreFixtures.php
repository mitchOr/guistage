<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Offre;
use App\Entity\User;

class OffreFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $roles[] = 'ROLE_USER';
        for ($i=1; $i<=5; $i++)
        {
            $user=new User();
            $user->setNom("usernom ".$i);
            $user->setPrenom('userprenom'.$i);
            $user->setEmail('user'.$i.'@gmail.com');
            $user->setPassword('123456');      
            $user->setRoles($roles);
            if($i<=3)
            {
                $user->settype('Etudiant');
            }
            else
            {
                $user->settype('Entreprise');
            }
            $offre=new Offre();
            $offre->setTitre("offre n°".$i)
                  ->setContenu("Contenu n° ".+$i)
                  ->setDate(new \DateTime())
                  ->setEmployeur($user)
                  ->setPaye("oui")
                  ->setLieu("Tours")
                  ->setSalaire("$600-$1200");

                  $manager->persist($offre);
        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
} 

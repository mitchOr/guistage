<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class RecruteurController extends AbstractController
{
    /**
     * @Route("/company_info", name="info_company")
     */
    public function read_company_info(): Response
    {
        return $this->render('recruteur/company-profile.html.twig');
    }
    
    /**
     * @Route("/change_password", name="password_change")
     */
    public function changePassword(): Response
    {
        return $this->render('recruteur/change-password.html.twig');
    }

    // /**
    //  * @Route("/jobs_manager", name="manage_jobs")
    //  */
    // public function managejob(AuthenticationUtils $authenticationUtils): Response
    // {
    //     return $this->render('etudiant/manage-jobs.html.twig');
    // }
   
    /**
     * @Route("/schedule_recruteur", name="recruteur_rdv")
     */
    // public function logout(AuthenticationUtils $authenticationUtils): Response
    // {}    

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(AuthenticationUtils $authenticationUtils): Response
    {}    
        
}

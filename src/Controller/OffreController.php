<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Offre;
use App\Entity\User;
use App\Form\OffreType;


class OffreController extends AbstractController
{
    /**
     * @Route("/offre", name="offre")
     */
    public function index()
    {
        //recupérer toutes les offres
        $repos = $this->getDoctrine()->getRepository(Offre::class);
        $offres= $repos->findAll();
        return $this->render('offre/index.html.twig', [
            'controller_name' => 'OffreController',
            'offres' => $offres
        ]);
    }


   

    /**
     * @Route("/user/offre/{id}", name="offre_show", requirements={"id"="\d+"})
     */
    public function show($id)
    {
        $repo=$this->getDoctrine()->getRepository(Offre::class);
        $repos = $this->getDoctrine()->getRepository(Offre::class);
        $offre=$repos->find($id);
        $entreprise=null;
        if($offre !== null)
        {
          $entreprise=$offre->getEmployeur();
        }
        
        return $this->render('offre/show.html.twig',[
            'offre' => $offre,
            'entreprise' => $entreprise,
        ]);
    }


    /**
     * @Route("/offre/new/{id}", name="offre_create")
     */
    public function create(Request $request, User $user): Response
    {
        $offre = new Offre();
        $form = $this->createForm(OffreType::class, $offre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
           
            $offre->setEmployeur($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($offre);
            $entityManager->flush();


            return $this->redirectToRoute('offre_show',['id' => $offre->getId()]);
        }

        return $this->render('offre/create.html.twig', [
            'OffreForm' => $form->createView(),
        ]);
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class PersonAccountController extends AbstractController
{
    /**
     * @Route("/person_info", name="info_person")
     */
    public function read_person_info(): Response
    {
        return $this->render('etudiant/candidate-profile.html.twig');
    }
    
    /**
     * @Route("/jobs_manager", name="manage_jobs")
     */
    public function managejob(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('etudiant/manage-jobs.html.twig');
    }

    // /**
    //  * @Route("/logout", name="offre_postuler")
    //  */
    // public function read_offre_postuler(AuthenticationUtils $authenticationUtils): Response
    // {}
   
    /**
     * @Route("/schedule_candidates", name="schedule")
     */
    public function logout(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('etudiant/calendrier.html.twig');
    }    

    /**
     * @Route("/logout", name="app_logout")
     */
    // public function logout(AuthenticationUtils $authenticationUtils): Response
    // {}  
        
    /**
     * @Route("/change_password", name="etudiant_password_change")
     */
    public function changePassword(): Response
    {
        return $this->render('etudiant/change-password.html.twig');
    }    
        
}

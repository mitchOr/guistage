<?php

namespace App\Controller;
use App\Entity\Offre;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @Route("/default", name="default")
     */
    public function index()
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        $repos = $this->getDoctrine()->getRepository(Offre::class);
        $offres= $repos->findAll();
        return $this->render('default/home.html.twig', [
            'controller_name' => 'DefaultController',
            'offres' => $offres
        ]);
    }

    /**
     * @Route("/home2", name="dashboard_recru")
     */
    public function home2()
    {
     
        return $this->render('default/dashboard_recru.html.twig');
    }

    /**
     * @Route("/user/recherche",name="rechercheStage")
     */
    public function recherche(Request $request)
    {
        $repos = $this->getDoctrine()->getRepository(Offre::class);
        $titre=$request->request->get('titre');
        $ville=$request->request->get('ville');

        if($titre=="" and $ville!="")
        {
            $offres= $repos->findAllByLieu($ville);
        }
        else if($titre!="" and $ville=="")
        {
            $offres= $repos-> findAllByTitre($titre);
        }
        else if($titre!="" and $ville!=""){
            $offres= $repos->findByField($ville, $titre);
        }
        else{
            $offres= $repos->findAll();
        }
        /*if( $offres==null)
        {
            $offres= $repos->findAll();
        }*/
   
        return $this->render('default/home.html.twig', [
            'controller_name' => 'DefaultController',
            'offres' => $offres
        ]);
    }

    /**
     * @Route("/etudiants", name="etudiants")
     */
    public function etudiants()
    {
        //on récupère la liste des étudiants
        $repos = $this->getDoctrine()->getRepository(User::class);
        $etudiants= $repos->findByType("Etudiant");
        return $this->render('default/etudiantsListe.html.twig', [
            'controller_name' => 'DefaultController',
            'etudiants' => $etudiants
        ]);
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190725222333 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, civilite VARCHAR(255) DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, prenom VARCHAR(255) DEFAULT NULL, date_de_naissance DATETIME DEFAULT NULL, type VARCHAR(255) NOT NULL, raison_social VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, adresse VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, pays VARCHAR(255) DEFAULT NULL, numero_telephone INT DEFAULT NULL, specialite VARCHAR(255) DEFAULT NULL, info LONGTEXT DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE offre DROP INDEX UNIQ_AF86866F5D7C53EC, ADD INDEX IDX_AF86866F5D7C53EC (employeur_id)');
        $this->addSql('ALTER TABLE offre ADD lieu VARCHAR(255) DEFAULT NULL, CHANGE salaire salaire VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE offre ADD CONSTRAINT FK_AF86866F5D7C53EC FOREIGN KEY (employeur_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE offre DROP FOREIGN KEY FK_AF86866F5D7C53EC');
        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE offre DROP INDEX IDX_AF86866F5D7C53EC, ADD UNIQUE INDEX UNIQ_AF86866F5D7C53EC (employeur_id)');
        $this->addSql('ALTER TABLE offre DROP lieu, CHANGE salaire salaire DOUBLE PRECISION DEFAULT NULL');
    }
}
